#### Version  0.5.4

Updated to meet Barclaycard accreditation test script.

### Added :
* Enter Supervisor Code box for refunds
* Cancel transaction function (triggered on 'Card entry' screen)
* 'signature OK?' check box
* Reference number box for refunds


#### Version  0.5.2

### Added :
* GUI field to enter ports 1 and 2
* GUI field to enter merchant number for Lane 3000
* Refund button for port 1 and 2
* 'ON PORT ****' postfix added to relevant messages

==============================================

#### Version  0.5.1

### Added :
* Lane 3000 compatibility

==============================================


#### Version  0.5.0

### Added :
* Dual terminal support

==============================================


#### Version  0.4.0

### Tested to EFT version: UT4.19.06

### Added :
* HTML handling for print requests
* Error handling for incorrect log directory
* Absolute path and browse button to log folder location
* takepayments colour themed welcome message

### Fixed:
* Font size in print requests
* OK button on connection menu (not functioning after logging menu closed)
* "Connected" repeatedly showing in output box when connection refreshed

==============================================


#### Version  0.3.5

* Rebuilt connection menu
* Streamlined save settings function

==============================================

#### Version  0.3.4

* logs sent message
* message appear as one entry in logs
* fixed public IP crash bug
* Myserver now EcrServer

==============================================

#### Version  0.3.3

* message now SerializeToString in sendMessgae()

==============================================

#### Version  0.3.2

* Fixed icons
* Added  detail to about aboutBox

==============================================

#### Version  0.3.0

* Rebuild User interface
